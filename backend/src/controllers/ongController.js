const crypto = require("crypto");
const connetion = require("../database/connection");

module.exports = {

 async index(req,resp){
        const ongs = await connetion("ongs").select("*");
        return resp.json(ongs);
 },

  async create(req,resp){
    const {name,email,whatsapp,city,uf}  = req.body; 

    const id = crypto.randomBytes(4).toString("HEX");


    await connetion("ongs").insert(
    {
        id,
        name,
        email,
        whatsapp,
        city,
        uf,
    });

    return resp.json({id});
    }
    
};

    


