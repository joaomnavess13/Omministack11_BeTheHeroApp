import React,{useState} from "react";
import { Link, useHistory } from "react-router-dom";
import { FiLogIn } from "react-icons/fi";
import "./style.css";
import logoImg from "../../assets/logo.svg";
import heroesImg from "../../assets/heroes.png";

import api from "../../services/api";

export default function Logon(){

    const [id, setId] = useState("");
    const history = useHistory();

    async function handleLogin(e){
        e.preventDefault();

        try{
            const resp = await api.post("sessions",{id });
            
            localStorage.setItem("ongId", id);
            localStorage.setItem("ongName", resp.data.name);

            history.push("/profile");

        }catch{
            alert("Falha no Login, Tente novamente");
        }
    }

    return(
        <div className="logon-container">
            <section className="form">
            <img src={logoImg} alt="Be The Hero Logo"/>
                      
            <form onSubmit={handleLogin}> 
                <h1>Faça seu Logon</h1>

                <input placeholder="Sua ID"
                value={id}
                onChange={e => setId(e.target.value)}
                />
                <button className="button" type="submit">Entrar</button>

                <Link className="backlink" to="/register">
                    <FiLogIn size={16} color="#e02041" />
                    Não Tem Cadastro
                </Link>
            </form>
            </section>

            <img src={heroesImg} alt="Heroes"/>
        </div>
    );
}