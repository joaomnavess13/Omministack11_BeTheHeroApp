import React, {useState,useEffect} from "react";
import {Link, useHistory} from "react-router-dom";
import {FiPower,FiTrash2} from "react-icons/fi";
import logoImg from "../../assets/logo.svg"
import "./style.css"

import api from "../../services/api";

export default function Profile(){

    const [incidents , setIncidents] = useState([]);
    const history = useHistory();

    const ongName = localStorage.getItem("ongName");
    const ongId = localStorage.getItem("ongId");
     
    useEffect (()=> {
        api.get("profile",{
            headers: {
                authorization: ongId,
            }            
        }).then(resp =>  {
            setIncidents(resp.data); 
        })
    }, [ongId]);

        async function handleDeleteIcidente(id){
            try{
             await api.delete(`incidents/${id}`,{
                 headers:{
                     authorization: ongId,
                 }
             });
             setIncidents(incidents.filter(incidents => incidents.id !== id));   
            }catch(erro){
                alert("Erro ao deleter caso,tente novamente");
            }
        }

        function handleLogout() {
            
            localStorage.clear();

            history.push("/");
            
        }
        
    return (

       

        <div className="profile-container">
            <header>
                <img src={logoImg} alt="Be The Hero Logo"/>
                <span>Bem Vindo,{ongName} </span>

                <Link className="button" to="/incident/new"> <p className="cadastro">Cadastrar novo Caso</p> </Link>

                <button onClick={handleLogout} type="button">
                    <FiPower size={18} color="#E02041"/>
                </button>
            </header>

            <h1>Casos Cadastrados</h1>

            <ul>
                {incidents.map(incidents => (
                    <li key={incidents.id}> 
                    <strong>CASO:</strong>
                    <p>{incidents.title} </p>

                    <strong>DESCRIÇÃO:</strong>
                    <p>{incidents.description}</p>

                    <strong>VALOR:</strong>
                    <p>{Intl.NumberFormat("pt-BR", {style: "currency",currency: "BRL"}).format(incidents.value)}</p>

                    <button onClick={() =>handleDeleteIcidente(incidents.id)} type="button">
                        <FiTrash2 size={20} color="#a8a8b3"/>
                    </button>
                </li>
                ))}                
            </ul>

        </div>

    );
    
}